package io.mineparty.control;

import com.google.common.base.Preconditions;
import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.control.configuration.ControlGameModeProvider;
import io.mineparty.control.game.WaitingState;
import io.mineparty.control.game.data.CDAchievements;
import io.mineparty.control.game.data.GameData;
import io.mineparty.control.game.data.Statistics;
import io.mineparty.control.util.CommonListener;
import io.mineparty.game.MPGame;
import io.mineparty.game.configuration.GameModeInformation;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Random;

public class ControlPlugin extends JavaPlugin {
    private static ControlPlugin plugin;
    private final Random random = new Random();
    private GameData gameData;

    public static final String SP = ChatColor.GRAY + ChatColor.BOLD.toString() + "CONTROL:" + ChatColor.RESET +
            ChatColor.GRAY + " ";

    public static ControlPlugin getPlugin() {
        return plugin;
    }

    @Override
    public void onEnable() {
        plugin = this;

        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.DISRUPTED);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.GOOD_MOURNING);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.STRONG_ALONE);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.PACIFIST);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.NO_HELP_REQUIRED);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.CHECK_THAT_POINT);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.CHECKING_IN);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.CHECKPOINT_MASTER);
        MinePartyCommons.getPlugin().getRegistry().registerAchievement(CDAchievements.CHECKMATE);

        MinePartyCommons.getPlugin().getRegistry().registerStatistic(Statistics.CD_CHECKPOINT_CAPTURES);
        MinePartyCommons.getPlugin().getRegistry().registerStatistic(Statistics.CD_CHECKPOINT_CAPTURES_ALONE);
        MinePartyCommons.getPlugin().getRegistry().registerStatistic(Statistics.CD_GAME_WINS);

        MinePartyCommons.getPlugin().getRegistry().commitAndMakeReadOnly();

        MPGame.getPlugin().setGameModeInformation(new GameModeInformation(this, new ControlGameModeProvider()));
        MPGame.getPlugin().getGameStateManager().changeState(new WaitingState());

        getServer().getPluginManager().registerEvents(new CommonListener(), this);
    }

    public Random getRandom() {
        return random;
    }

    public GameData getGameData() {
        return gameData;
    }

    public void setGameData(GameData gameData) {
        Preconditions.checkState(gameData != null, "game data already set");
        this.gameData = gameData;
    }
}
