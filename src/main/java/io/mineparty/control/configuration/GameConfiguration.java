package io.mineparty.control.configuration;

import io.mineparty.control.game.data.CDTeam;
import io.mineparty.lib.util.ConfigurationSectionUtil;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.stream.Collectors;

public class GameConfiguration {
    private final List<Checkpoint> checkpoints = new ArrayList<>();
    private final Map<CDTeam, TeamSettings> teams = new EnumMap<>(CDTeam.class);

    public GameConfiguration(ConfigurationSection section) {
        ConfigurationSection checkpointSection = ConfigurationSectionUtil.checkSection(section, "checkpoints");
        checkpointSection.getKeys(false).stream()
                .map(checkpointSection::getConfigurationSection)
                .map(this::deserializeCheckpoint)
                .forEach(checkpoints::add);

        if (checkpoints.isEmpty()) {
            throw new RuntimeException("No checkpoints defined.");
        }

        for (CDTeam team : CDTeam.values()) {
            ConfigurationSection teamSection = ConfigurationSectionUtil.checkSection(section, "team." + team.name());
            teams.put(team, deserializeTeamSettings(teamSection));
        }
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public Map<CDTeam, TeamSettings> getTeams() {
        return teams;
    }

    private Checkpoint deserializeCheckpoint(ConfigurationSection section) {
        Map<String, Object> map = ConfigurationSectionUtil.checkSection(section, "base").getValues(false);
        int maxHealth = section.getInt("max-health", 30);
        double captureDistance = section.getDouble("capture-distance", 4);

        return new Checkpoint(Vector.deserialize(map), maxHealth, captureDistance);
    }

    private TeamSettings deserializeTeamSettings(ConfigurationSection section) {
        ConfigurationSection allSpawns = ConfigurationSectionUtil.checkSection(section, "spawns");
        ConfigurationSection initialSpawnSection = ConfigurationSectionUtil.checkSection(allSpawns, "initial");
        ConfigurationSection midGameSection = ConfigurationSectionUtil.checkSection(allSpawns, "mid-game");

        List<Vector> initialSpawns = initialSpawnSection.getKeys(false).stream()
                .map(initialSpawnSection::getConfigurationSection)
                .map(s -> Vector.deserialize(s.getValues(false)))
                .collect(Collectors.toList());

        if (initialSpawns.isEmpty()) {
            throw new RuntimeException("initial spawns missing for " + section.getCurrentPath());
        }

        return new TeamSettings(initialSpawns, Vector.deserialize(midGameSection.getValues(false)));
    }

    public static class Checkpoint {
        private final Vector base;
        private final int maxHealth;
        private final double captureDistance;

        public Checkpoint(Vector base, int maxHealth, double captureDistance) {
            this.base = base;
            this.maxHealth = maxHealth;
            this.captureDistance = captureDistance;
        }

        public Vector getBase() {
            return base.clone();
        }

        public int getMaxHealth() {
            return maxHealth;
        }

        public double getCaptureDistance() {
            return captureDistance;
        }
    }

    public static class TeamSettings {
        private final List<Vector> initialSpawns;
        private final Vector teamSpawn;

        public TeamSettings(List<Vector> initialSpawns, Vector teamSpawn) {
            this.initialSpawns = initialSpawns;
            this.teamSpawn = teamSpawn;
        }

        public List<Vector> getInitialSpawns() {
            return initialSpawns.stream().map(Vector::clone).collect(Collectors.toList());
        }

        public Vector getTeamSpawn() {
            return teamSpawn.clone();
        }
    }
}
