package io.mineparty.control.configuration;

import io.mineparty.control.game.PlayingState;
import io.mineparty.game.GameState;
import io.mineparty.game.configuration.GameModeProvider;
import io.mineparty.game.configuration.MapConfiguration;
import org.bukkit.configuration.ConfigurationSection;

public class ControlGameModeProvider implements GameModeProvider {
    @Override
    public Object deserializeGameData(ConfigurationSection configurationSection) {
        return new GameConfiguration(configurationSection);
    }

    @Override
    public GameState createFirstGameState(MapConfiguration mapConfiguration) {
        return new PlayingState();
    }
}
