package io.mineparty.control.game;

import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.event.GameFinishEvent;
import io.mineparty.control.game.data.CDTeam;
import io.mineparty.game.GameState;
import io.mineparty.game.extras.states.endgame.CollectionWinnerFetcher;
import io.mineparty.game.extras.states.endgame.TeamWinnerFetcher;
import org.bukkit.Bukkit;

import java.util.stream.Collectors;

public class EndGameState extends io.mineparty.game.extras.states.endgame.EndGameState {
    private final CDTeam winner;

    public EndGameState(CDTeam winner) {
        super(new TeamWinnerFetcher(ControlPlugin.getPlugin().getGameData().getScoreboardTeam(winner)),
                ControlPlugin.getPlugin().getGameData().getRewardManager());
        this.winner = winner;
    }

    @Override
    public void onStart(GameState previous) {
        super.onStart(previous);

        Bukkit.getPluginManager().callEvent(new GameFinishEvent(Bukkit.getOnlinePlayers().stream()
                .filter(p -> ControlPlugin.getPlugin().getGameData().getPlayerTeam(p) == winner)
                .map(MinePartyCommons.getPlugin()::getPlayer)
                .collect(Collectors.toList())));
    }
}
