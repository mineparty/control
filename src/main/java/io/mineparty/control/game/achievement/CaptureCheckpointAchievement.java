package io.mineparty.control.game.achievement;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import io.mineparty.commons.core.data.Achievement;
import io.mineparty.control.event.CheckpointCaptureEvent;
import io.mineparty.control.game.data.Statistics;
import io.mineparty.game.extras.rewards.GameRewardManager.Reward;

public class CaptureCheckpointAchievement extends CDAchievement implements Listener {
    
    protected final int amountToCapture;
    
    public CaptureCheckpointAchievement(Achievement achievement, int amountToCapture, AchievementReward[] rewards) {
        super(achievement, Statistics.CD_CHECKPOINT_CAPTURES, rewards);
        this.amountToCapture = amountToCapture;
    }
    
    @EventHandler
    public void onCheckpointCapture(CheckpointCaptureEvent event) {
        event.getPlayers().stream().filter(player -> !player.getAllAchievements().stream().anyMatch(a -> a.getAchievement().equals(achievement))).forEach(player -> {
            int current = getStatisticValue(player);
            if (current + 1 >= this.amountToCapture) {
                reward(player);
            }
        });
    
    }
}