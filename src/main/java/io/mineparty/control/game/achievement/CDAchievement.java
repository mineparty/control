package io.mineparty.control.game.achievement;

import io.mineparty.lib.util.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import io.mineparty.commons.core.MinePartyPlayer;
import io.mineparty.commons.core.data.Achievement;
import io.mineparty.commons.core.data.Statistic;
import io.mineparty.control.ControlPlugin;
import io.mineparty.game.extras.rewards.GameRewardManager.Reward;

public abstract class CDAchievement {
    
    protected final Achievement achievement;
    private final Statistic statistic;
    private final AchievementReward[] rewards;
    
    public CDAchievement(Achievement achievement, Statistic statistic, AchievementReward[] rewards) {
        this.achievement = achievement;
        this.statistic = statistic;
        this.rewards = rewards;
        if (this instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) this, ControlPlugin.getPlugin());
        }
    }
    
    protected int getStatisticValue(MinePartyPlayer player) {
        return player.getStatistics().count(statistic);
    }
    
    protected void increaseStatisticValue(MinePartyPlayer player, int amount) {
        Bukkit.getScheduler().runTaskAsynchronously(ControlPlugin.getPlugin(), () -> player.increaseStatistic(statistic, amount));
    }
    
    protected void reward(MinePartyPlayer player) {
        PlayerUtil.addAchievement(Bukkit.getPlayer(player.getMinecraftUuid()), achievement, res -> {
            for (AchievementReward reward : rewards) {
                if (reward.getCoins() > 0) {
                    player.addCoins(reward.getCoins());
                }
            }
        });
    }

}
