package io.mineparty.control.game.achievement;

import io.mineparty.control.event.GameFinishEvent;
import io.mineparty.control.game.data.Statistics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import io.mineparty.commons.core.data.Achievement;
import io.mineparty.game.extras.rewards.GameRewardManager.Reward;

public class WinGameAchievement extends CDAchievement implements Listener {

    private final int amountToWin;
    public WinGameAchievement(Achievement achievement, int amountToWin, AchievementReward[] rewards) {
        super(achievement, Statistics.CD_GAME_WINS, rewards);
        this.amountToWin = amountToWin;
    }
    
    @EventHandler
    public void onGameWin(GameFinishEvent event) {
        event.getWinners().stream().filter(player -> !player.getAllAchievements().stream().anyMatch(a -> a.getAchievement().equals(this.achievement))).forEach(player -> {
            
            int current = getStatisticValue(player);
            current++;
            increaseStatisticValue(player, 1);
            if (current >= this.amountToWin) {
                reward(player);
            }
        });
    }

}