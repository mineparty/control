package io.mineparty.control.game.achievement;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AchievementReward {
    private final long coins;
}
