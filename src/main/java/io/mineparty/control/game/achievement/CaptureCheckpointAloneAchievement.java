package io.mineparty.control.game.achievement;

import org.bukkit.event.EventHandler;

import io.mineparty.commons.core.MinePartyPlayer;
import io.mineparty.commons.core.data.Achievement;
import io.mineparty.control.event.CheckpointCaptureEvent;
import io.mineparty.control.game.data.Statistics;
import io.mineparty.game.extras.rewards.GameRewardManager.Reward;
import org.bukkit.event.Listener;

/**
 * @author Kenny
 *
 */
public class CaptureCheckpointAloneAchievement extends CDAchievement implements Listener {
    
    private final int amountToCapture;

    public CaptureCheckpointAloneAchievement(Achievement achievement, int amountToCapture, AchievementReward[] rewards) {
        super(achievement, Statistics.CD_CHECKPOINT_CAPTURES_ALONE, rewards);
        this.amountToCapture = amountToCapture;
    }
    
    @EventHandler
    public void onCheckpointCapture(CheckpointCaptureEvent event) {
        if (event.getPlayers().size() != 1) {
            return;
        }
        
        MinePartyPlayer player = event.getPlayers().get(0);
        if (player.getAllAchievements().stream().anyMatch(a -> a.getAchievement().equals(achievement))) {
            return;
        }
        
        int current = getStatisticValue(player);
        current++;
        increaseStatisticValue(player, 1);
        if (current >= this.amountToCapture) {
            reward(player);
        }
    }

}
