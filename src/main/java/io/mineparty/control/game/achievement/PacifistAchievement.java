package io.mineparty.control.game.achievement;

import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.event.GameFinishEvent;
import io.mineparty.control.game.data.CDAchievements;
import io.mineparty.control.game.data.CDRewards;
import io.mineparty.game.extras.rewards.GameRewardManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PacifistAchievement extends CDAchievement implements Listener  {
    public PacifistAchievement(AchievementReward[] rewards) {
        super(CDAchievements.PACIFIST, null, rewards);
    }

    @EventHandler
    public void onGameWin(GameFinishEvent event) {
        Bukkit.getOnlinePlayers().stream()
                .filter(player -> ControlPlugin.getPlugin().getGameData().getRewardManager().countRewards(player, CDRewards.KILLS) == 0)
                .map(MinePartyCommons.getPlugin()::getPlayer)
                .forEach(this::reward);
    }
}
