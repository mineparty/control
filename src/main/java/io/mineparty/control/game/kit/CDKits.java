package io.mineparty.control.game.kit;

import com.google.common.collect.ImmutableList;
import io.mineparty.game.extras.kit.KitInfo;
import io.mineparty.lib.ItemBuilder;
import lombok.experimental.UtilityClass;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

@UtilityClass
public class CDKits {
    public static final KitInfo SOLDIER = KitInfo.builder()
            .name("Soldier")
            .defaultKit(true)
            .description(ImmutableList.of(ChatColor.WHITE + "Solemn and at service.", "",
                    ChatColor.GOLD + "Default kit", ChatColor.GRAY + "Leather armor", ChatColor.GRAY + "Stone sword"))
            .item(new ItemBuilder(Material.STONE_SWORD).setTitle("Soldier"))
            .onApply(player -> {
                player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
                player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
                player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
                player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
                player.getInventory().addItem(new ItemStack(Material.STONE_SWORD));
            })
            .build();

    public static final KitInfo SCOUT = KitInfo.builder()
            .name("Scout")
            .description(ImmutableList.of(ChatColor.WHITE + "Nimble but packs little protection.", "",
                    ChatColor.GOLD + "Speed II",
                    ChatColor.GRAY + "Gold chestplate/leggings",
                    ChatColor.GRAY + "Wood sword"))
            .item(new ItemBuilder(Material.GOLD_HELMET).setTitle("Scout"))
            .onApply(player -> {
                player.getInventory().setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
                player.getInventory().setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
                player.getInventory().addItem(new ItemStack(Material.WOOD_SWORD));
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1, true, false));
            })
            .build();

    public static final KitInfo TANK = KitInfo.builder()
            .name("Tank")
            .defaultKit(true)
            .description(ImmutableList.of(ChatColor.WHITE + "Strong, slow, but doesn't sweat.", "",
                    ChatColor.RED + "Slowness I",
                    ChatColor.GRAY + "Iron chestplate/boots",
                    ChatColor.GRAY + "Wood axe"))
            .item(new ItemBuilder(Material.IRON_CHESTPLATE).setTitle("Tank"))
            .onApply(player -> {
                player.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE));
                player.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS));
                player.getInventory().addItem(new ItemStack(Material.WOOD_AXE));
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 0, true, false));
            })
            .build();

    public static final KitInfo ARCHER = KitInfo.builder()
            .name("Soldier")
            .defaultKit(true)
            .description(ImmutableList.of(ChatColor.WHITE + "Swift and crafty with a bow.", "",
                    ChatColor.GRAY + "Leather armor",
                    ChatColor.GRAY + "Bow and 15 arrows"))
            .item(new ItemBuilder(Material.BOW).setTitle("Archer"))
            .onApply(player -> {
                player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
                player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
                player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
                player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
                player.getInventory().addItem(new ItemStack(Material.BOW));
                player.getInventory().addItem(new ItemStack(Material.ARROW, 15));
            })
            .build();

    public static ItemStack getKitItem() {
        ItemStack stack = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Change Kit");
        meta.setLore(ImmutableList.of(ChatColor.WHITE + "Right-click to change your kit."));
        stack.setItemMeta(meta);
        return stack;
    }
}
