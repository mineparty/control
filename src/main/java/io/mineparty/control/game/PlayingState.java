package io.mineparty.control.game;

import com.darkblade12.particleeffect.ParticleEffect;
import com.google.common.base.Preconditions;
import com.google.common.collect.*;
import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.event.CheckpointCaptureEvent;
import io.mineparty.control.game.data.CDRewards;
import io.mineparty.control.game.data.GameData;
import io.mineparty.control.game.data.Statistics;
import io.mineparty.control.game.kit.CDKits;
import io.mineparty.control.util.BlockUtil;
import io.mineparty.control.configuration.GameConfiguration;
import io.mineparty.control.game.data.CDTeam;
import io.mineparty.cosmetics.MPCosmetics;
import io.mineparty.game.GameState;
import io.mineparty.game.MPGame;
import io.mineparty.lib.mechanics.RespawnWave;
import io.mineparty.lib.util.CenteredText;
import io.mineparty.lib.util.PlayerUtil;
import io.mineparty.lib.util.Title;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.stream.Collectors;

public class PlayingState implements GameState {
    private static final String CHECK = "\u2714";
    private static final String NO = "\u2718";

    private static final int GAME_TIME = 180;

    private boolean gameStarted = false;
    private final WaitingData waitingData = new WaitingData();
    private final RespawnData respawnData = new RespawnData();
    private RespawnWave respawnWave;
    private int timeLeft = GAME_TIME;
    private BukkitTask tickTask;

    @Override
    public void onStart(GameState previous) {
        if (!(previous instanceof WaitingState)) {
            throw new IllegalStateException("Game started from unknown state: " + previous);
        }

        for (Map.Entry<CDTeam, UUID> entry : ((WaitingState) previous).getTeamGUI().getMembers().entries()) {
            Player player = Bukkit.getPlayer(entry.getValue());
            if (player != null) {
                ControlPlugin.getPlugin().getGameData().putOnTeam(player, entry.getKey());
            }
        }

        Bukkit.getOnlinePlayers().forEach(this::setupPlayer);
        respawnWave = new RespawnWave(6, this::setupPlayer);

        ControlPlugin.getPlugin().getGameData().getObjective().getScore("Checkpoints:")
                .setScore(ControlPlugin.getPlugin().getGameData().getCurrentMap().getCheckpoints().size());

        ControlPlugin.getPlugin().getGameData().getCurrentMap().getCheckpoints().forEach(this::redrawCheckpoint);
        tickTask = Bukkit.getScheduler().runTaskTimer(ControlPlugin.getPlugin(), this::tick, 0, 20);
    }

    @Override
    public void onStop(GameState next) {
        tickTask.cancel();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        setupPlayer(event.getPlayer());
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        CDTeam team = ControlPlugin.getPlugin().getGameData().getPlayerTeam(player);
        if (team == null) team = ControlPlugin.getPlugin().getGameData().assignTeam(player);

        GameConfiguration.TeamSettings teamSettings = ControlPlugin.getPlugin().getGameData().getCurrentMap().getTeams().get(team);

        // Doing this again is safe, otherwise the game would have hung earlier!
        Vector spawn;
        do {
            spawn = teamSettings.getInitialSpawns().get(ControlPlugin.getPlugin().getRandom().nextInt(teamSettings.getInitialSpawns().size()));
        } while (respawnData.used.containsValue(spawn));
        respawnData.used.put(event.getPlayer().getUniqueId(), spawn);
        Block block = spawn.toLocation(ControlPlugin.getPlugin().getGameData().getWorld()).getBlock().getRelative(BlockFace.DOWN);

        MPCosmetics.getPlugin().spawnSpawnBox(player, block.getLocation());

        event.setRespawnLocation(spawn.toLocation(ControlPlugin.getPlugin().getGameData().getWorld()));
        respawnWave.enterWave(player);
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        event.setDroppedExp(0);
        event.getDrops().clear();

        ControlPlugin.getPlugin().getGameData().getRewardManager().addReward(event.getEntity(), CDRewards.DEATHS);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (!gameStarted) {
            event.setCancelled(true);

            if (event.getMaterial() == Material.IRON_SWORD) {
                ControlPlugin.getPlugin().getGameData().getKitManager().openMenu(MinePartyCommons.getPlugin()
                        .getPlayer(event.getPlayer()));
            }
        }
    }

    private void setHotbarIndicator(Player player) {
        CDTeam team = ControlPlugin.getPlugin().getGameData().getPlayerTeam(player);

        ItemStack stack = new Wool(Material.STAINED_CLAY, team.getDyeColor().getWoolData()).toItemStack(1);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(team.getColor() + team.getName());
        stack.setItemMeta(meta);
        player.getInventory().setItem(8, stack);
    }

    private void setupPlayer(Player player) {
        PlayerUtil.resetPlayer(player);
        player.setNoDamageTicks(60);

        CDTeam team = ControlPlugin.getPlugin().getGameData().getPlayerTeam(player);
        if (team == null) {
            team = ControlPlugin.getPlugin().getGameData().assignTeam(player);
        }

        for (int i = 0; i < 2; i++) {
            player.sendMessage("");
        }
        CenteredText.sendCenteredMessage(player, "You are on the " + team + ChatColor.RESET + " team.");
        CenteredText.sendCenteredMessage(player, ChatColor.GOLD + ChatColor.BOLD.toString() + "GOAL:");
        CenteredText.sendCenteredMessage(player, team.getGoal());
        for (int i = 0; i < 2; i++) {
            player.sendMessage("");
        }

        GameConfiguration.TeamSettings teamSettings = ControlPlugin.getPlugin().getGameData().getCurrentMap().getTeams().get(team);

        if (gameStarted) {
            Vector vector = respawnData.used.remove(player.getUniqueId());
            if (vector != null) {
                Block block = vector.toLocation(ControlPlugin.getPlugin().getGameData().getWorld()).getBlock().getRelative(BlockFace.DOWN);
                block.setType(Material.AIR);
            } else {
                player.teleport(teamSettings.getTeamSpawn().toLocation(ControlPlugin.getPlugin().getGameData().getWorld()));
            }
            ControlPlugin.getPlugin().getGameData().getKitManager().applyKit(MinePartyCommons.getPlugin().getPlayer(player));
            setHotbarIndicator(player);
        } else {
            if (Bukkit.getOnlinePlayers().size() > teamSettings.getInitialSpawns().size()) {
                player.kickPlayer("There's not enough room to spawn you!");
                return;
            }
            Vector spawn;
            do {
                spawn = teamSettings.getInitialSpawns().get(ControlPlugin.getPlugin().getRandom().nextInt(teamSettings.getInitialSpawns().size()));
            } while (waitingData.getUsed().contains(spawn));
            waitingData.getUsed().add(spawn);
            Location location = spawn.toLocation(ControlPlugin.getPlugin().getGameData().getWorld());
            player.teleport(location);
            MPCosmetics.getPlugin().spawnSpawnBox(player, location.getBlock().getRelative(BlockFace.DOWN).getLocation());
            player.getInventory().addItem(CDKits.getKitItem());
        }

        player.setScoreboard(ControlPlugin.getPlugin().getGameData().getScoreboard());
    }

    private void tick() {
        if (Bukkit.getOnlinePlayers().isEmpty()) {
            Bukkit.shutdown();
            return;
        }

        if (!gameStarted) {
            waitingData.countdown--;

            if (waitingData.countdown == 10 || (waitingData.countdown <= 5 && waitingData.countdown != 0)) {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    String end = waitingData.countdown <= 3 ? (waitingData.countdown != 1 ? "s" : "") + "!" : "s.";
                    player.sendMessage("The match begins in " + ChatColor.GOLD + waitingData.countdown + ChatColor.RESET + " second" + end);
                    player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 10, 2);
                });
            } else if (waitingData.countdown == 0) {
                // Release the players. Give them a short period of being invincible.
                for (Vector vector : waitingData.getUsed()) {
                    Block block = vector.toLocation(ControlPlugin.getPlugin().getGameData().getWorld()).getBlock().getRelative(BlockFace.DOWN);
                    block.setType(Material.AIR);
                }
                waitingData.getUsed().clear();
                Bukkit.getOnlinePlayers().forEach(p -> {
                    p.setNoDamageTicks(60);
                    if (p.getOpenInventory() != null) p.closeInventory();
                    p.getInventory().clear();
                    ControlPlugin.getPlugin().getGameData().getKitManager().applyKit(MinePartyCommons.getPlugin().getPlayer(p));
                    setHotbarIndicator(p);
                });
                gameStarted = true;
            }

            return;
        }

        String prefix = ControlPlugin.SP + ChatColor.BOLD + DurationFormatUtils.formatDuration(timeLeft * 1000, "mm:ss");
        ControlPlugin.getPlugin().getGameData().getObjective().setDisplayName(prefix);

        // Iterate over all checkpoints and reduce health as needed.
        List<Player> nearCheckpoints = new ArrayList<>();
        int enabledCheckpoints = 0;

        for (GameConfiguration.Checkpoint checkpoint : ControlPlugin.getPlugin().getGameData().getCurrentMap().getCheckpoints()) {
            GameData.CheckpointState checkpointState = ControlPlugin.getPlugin().getGameData().getCheckpointState(checkpoint);

            if (checkpointState.isDestroyed()) {
                continue;
            }

            Multiset<CDTeam> playerSet = EnumMultiset.create(CDTeam.class);
            List<Player> inZone = Bukkit.getOnlinePlayers().stream()
                    .filter(player -> {
                        // TODO: Extra range checking.
                        Vector pVec = player.getLocation().toVector();
                        if (pVec.getBlockY() < checkpoint.getBase().getBlockY()) return false;
                        pVec.setY(checkpoint.getBase().getBlockY());
                        return pVec.distance(checkpoint.getBase()) <= checkpoint.getCaptureDistance();
                    })
                    .collect(Collectors.toList());
            nearCheckpoints.addAll(inZone);

            for (Player player : inZone) {
                CDTeam team = ControlPlugin.getPlugin().getGameData().getPlayerTeam(player);
                if (playerSet.count(team) < 3)
                    playerSet.add(team);
            }

            if (playerSet.contains(CDTeam.SAVAGE)) {
                // If there's any savages around, consider it an "attack" so the auto-healer doesn't kick in.
                checkpointState.setLastAttack(timeLeft);
            }

            int effectiveDamage = playerSet.count(CDTeam.SAVAGE) - playerSet.count(CDTeam.PROTECTOR);
            if (effectiveDamage > 0) {
                checkpointState.setHealth(Math.max(0, checkpointState.getHealth() - effectiveDamage));
                redrawCheckpoint(checkpoint, true);

                if (checkpointState.isDestroyed()) {
                    // Destroyed checkpoint. That means we need some explosions!
                    List<Block> wools = BlockUtil.searchUpwards(checkpoint.getBase().toLocation(ControlPlugin.getPlugin().getGameData().getWorld()).getBlock(), Material.WOOL);
                    for (int i = 0; i < wools.size(); i++) {
                        Block wool = wools.get(i);
                        Bukkit.getScheduler().runTaskLater(ControlPlugin.getPlugin(), () -> {
                            wool.setType(Material.AIR);
                            wool.getWorld().playSound(wool.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 10, 1);
                            wool.getWorld().playEffect(wool.getLocation(), Effect.EXPLOSION_HUGE, 0);
                        }, (wools.size() - i) * 8);
                    }

                    Bukkit.getOnlinePlayers().forEach(p -> {
                        for (Player player : ControlPlugin.getPlugin().getGameData().getPlayersOn(CDTeam.PROTECTOR)) {
                            player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_INFECT, 3, 1);

                            Title.builder()
                                    .title("")
                                    .subtitle(ChatColor.RED + ChatColor.BOLD.toString() + NO + " CHECKPOINT DOWN!")
                                    .build()
                                    .display(p);
                        }
                        p.sendMessage("");
                        CenteredText.sendCenteredMessage(p, ChatColor.RED + ChatColor.BOLD.toString() + "A checkpoint has been destroyed!");
                        p.sendMessage("");
                    });

                    inZone.stream()
                            .filter(player -> ControlPlugin.getPlugin().getGameData().getPlayerTeam(player) == CDTeam.SAVAGE)
                            .forEach(player -> {
                                Title.builder()
                                    .title("")
                                    .subtitle(ChatColor.DARK_GREEN + CHECK + " CHECKPOINT DOWN")
                                    .build()
                                    .display(player);
                                PlayerUtil.incrementStatisticAsync(player, Statistics.CD_CHECKPOINT_CAPTURES);
                            });

                    Bukkit.getPluginManager().callEvent(new CheckpointCaptureEvent(checkpoint, inZone.stream()
                            .map(MinePartyCommons.getPlugin()::getPlayer).collect(Collectors.toList())));

                    continue;
                }
            } else if (!playerSet.contains(CDTeam.SAVAGE)) {
                if (checkpointState.getLastAttack() != -1) {
                    int lastTime = checkpointState.getLastAttack() - timeLeft;
                    if (lastTime % 5 == 0) {
                        checkpointState.setHealth(Math.min(checkpoint.getMaxHealth(), checkpointState.getHealth() + 1));
                        redrawCheckpoint(checkpoint, true);
                    }
                }
            }

            if (!checkpointState.isDestroyed()) {
                // Construct an action bar message for this checkpoint
                StringBuilder builder = new StringBuilder();

                int pctage = (int) (((double) checkpointState.getHealth() / checkpoint.getMaxHealth()) * 100);

                ChatColor color = pctage <= 25 ? ChatColor.DARK_RED : (pctage >= 75 ? ChatColor.GREEN : ChatColor.YELLOW);

                builder.append(ChatColor.WHITE)
                        .append(ChatColor.BOLD)
                        .append("Checkpoint: ")
                        .append(color)
                        .append(pctage)
                        .append(ChatColor.WHITE)
                        .append("% health");

                for (Player player : inZone) {
                    PlayerUtil.sendActionBar(player, builder.toString());
                }
            }

            enabledCheckpoints++;
        }

        Bukkit.getOnlinePlayers().stream()
                .filter(p -> !nearCheckpoints.contains(p))
                .forEach(p -> PlayerUtil.sendActionBar(p, ""));

        respawnWave.run();

        ControlPlugin.getPlugin().getGameData().getObjective().getScore("Checkpoints:").setScore(enabledCheckpoints);

        Multiset<CDTeam> allTeams = EnumMultiset.create(CDTeam.class);
        Bukkit.getOnlinePlayers().forEach(p -> allTeams.add(ControlPlugin.getPlugin().getGameData().getPlayerTeam(p)));

        if (allTeams.elementSet().size() == 1) {
            MPGame.getPlugin().getGameStateManager().changeState(new EndGameState(Iterables.getOnlyElement(allTeams.elementSet())));
            return;
        }

        if (enabledCheckpoints == 0) {
            MPGame.getPlugin().getGameStateManager().changeState(new EndGameState(CDTeam.SAVAGE));
        } else if (timeLeft <= 0) {
            MPGame.getPlugin().getGameStateManager().changeState(new EndGameState(CDTeam.PROTECTOR));
        }

        timeLeft--;
    }

    private void redrawCheckpoint(GameConfiguration.Checkpoint checkpoint) {
        redrawCheckpoint(checkpoint, false);
    }

    private void redrawCheckpoint(GameConfiguration.Checkpoint checkpoint, boolean playEffect) {
        GameData.CheckpointState checkpointState = ControlPlugin.getPlugin().getGameData().getCheckpointState(checkpoint);

        List<Block> wools = BlockUtil.searchUpwards(checkpoint.getBase().toLocation(ControlPlugin.getPlugin().getGameData().getWorld()).getBlock(), Material.WOOL);
        Preconditions.checkState(!wools.isEmpty(), "no wool found?!?");

        // Need to stretch taken health out over the entire cell.
        // i.e. 1/15 = 0.2, 0.2 * 5 = 1.
        //      5/15 = 0.33, 0.33 * 5 = 1.6, round up = 2 blocks.
        double damage = checkpoint.getMaxHealth() - checkpointState.getHealth();
        int savageBlocks = damage == 0 ? 0 : (int) Math.floor((damage / checkpoint.getMaxHealth()) * wools.size());

        // Get the current composition of blocks to see if we want to play effects.
        if (playEffect) {
            Multimap<CDTeam, Block> oldSet = ArrayListMultimap.create();
            Multimap<CDTeam, Block> newSet = ArrayListMultimap.create();
            for (Block wool : wools) {
                if (wool.getData() == CDTeam.SAVAGE.getDyeColor().getWoolData()) {
                    oldSet.put(CDTeam.SAVAGE, wool);
                } else if (wool.getData() == CDTeam.PROTECTOR.getDyeColor().getWoolData()) {
                    oldSet.put(CDTeam.PROTECTOR, wool);
                }
            }
            newSet.putAll(CDTeam.SAVAGE, wools.subList(0, savageBlocks));
            newSet.putAll(CDTeam.PROTECTOR, wools.subList(savageBlocks, wools.size()));
            if (oldSet.keys().count(CDTeam.SAVAGE) > savageBlocks) {
                // Losing to protector blocks
                List<Block> diff = new ArrayList<>(newSet.get(CDTeam.PROTECTOR));
                diff.retainAll(oldSet.get(CDTeam.SAVAGE));
                for (Block block : diff) {
                    block.getWorld().playSound(block.getLocation(), Sound.BLOCK_CLOTH_BREAK, 10F, 2F);
                    ParticleEffect.BLOCK_DUST.display(new ParticleEffect.BlockData(Material.WOOL, CDTeam.PROTECTOR.getDyeColor().getWoolData()), 0.5F, 0.5F, 0.3F, 0.1F, 16, block.getLocation(), 24);
                }
            } else if (oldSet.keys().count(CDTeam.SAVAGE) < savageBlocks) {
                // Losing to savage blocks
                List<Block> diff = new ArrayList<>(newSet.get(CDTeam.SAVAGE));
                diff.retainAll(oldSet.get(CDTeam.PROTECTOR));
                for (Block block : diff) {
                    block.getWorld().playSound(block.getLocation(), Sound.BLOCK_CLOTH_BREAK, 10F, 1F);
                    ParticleEffect.BLOCK_DUST.display(new ParticleEffect.BlockData(Material.WOOL, CDTeam.SAVAGE.getDyeColor().getWoolData()), 0.5F, 0.5F, 0.3F, 0.1F, 16, block.getLocation(), 24);
                }
            }
        }

        wools.subList(0, savageBlocks).forEach(block -> block.setData(CDTeam.SAVAGE.getDyeColor().getWoolData()));
        wools.subList(savageBlocks, wools.size()).forEach(block -> block.setData(CDTeam.PROTECTOR.getDyeColor().getWoolData()));
    }

    private class WaitingData {
        private final Set<Vector> used = new HashSet<>();
        private int countdown = 11;

        public Set<Vector> getUsed() {
            return used;
        }
    }

    private class RespawnData {
        private final Map<UUID, Vector> used = new HashMap<>();
    }
}
