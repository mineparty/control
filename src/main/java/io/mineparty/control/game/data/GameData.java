package io.mineparty.control.game.data;

import com.google.common.base.Preconditions;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.configuration.GameConfiguration;
import io.mineparty.control.game.kit.CDKits;
import io.mineparty.game.configuration.MapConfiguration;
import io.mineparty.game.extras.kit.KitManager;
import io.mineparty.game.extras.rewards.GameRewardManager;
import io.mineparty.lib.util.BrandingUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;
import java.util.stream.Collectors;

public class GameData {
    private final GameConfiguration currentMap;
    private final World world;

    private final Scoreboard scoreboard;
    private final Objective objective;
    private final Map<CDTeam, GameTeam> teamMap = new EnumMap<>(CDTeam.class);
    private final Map<GameConfiguration.Checkpoint, CheckpointState> checkpointMap = new HashMap<>();
    private final KitManager kitManager;
    private final GameRewardManager rewardManager = new GameRewardManager();

    public GameData(MapConfiguration currentMap, World world) {
        if (currentMap.getGameData() instanceof GameConfiguration) {
            this.currentMap = (GameConfiguration) currentMap.getGameData();
        } else {
            throw new RuntimeException("Extra data provided isn't from Control, was " + currentMap.getGameData().getClass().getName());
        }
        this.world = world;

        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreboard.registerNewObjective("CoreDestruction", "dummy");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.objective.setDisplayName(ControlPlugin.SP + ChatColor.BOLD + "Waiting...");
        this.objective.getScore(ChatColor.GRAY + "").setScore(-1);
        this.objective.getScore(BrandingUtil.IP_BOLD).setScore(-2);

        for (CDTeam team : CDTeam.values()) {
            Team bukkitTeam = scoreboard.registerNewTeam(team.name());
            bukkitTeam.setPrefix(team.getColor().toString());
            bukkitTeam.setAllowFriendlyFire(false);
            bukkitTeam.setDisplayName(team.getColor() + team.getName());
            teamMap.put(team, new GameTeam(bukkitTeam, team));
        }

        for (GameConfiguration.Checkpoint checkpoint : this.currentMap.getCheckpoints()) {
            checkpointMap.put(checkpoint, new CheckpointState(checkpoint));
        }

        kitManager = new KitManager();
        kitManager.addKit(CDKits.SOLDIER);
        kitManager.addKit(CDKits.SCOUT);
        kitManager.addKit(CDKits.TANK);
        kitManager.addKit(CDKits.ARCHER);
    }

    public GameConfiguration getCurrentMap() {
        return currentMap;
    }

    public World getWorld() {
        return world;
    }

    public KitManager getKitManager() {
        return kitManager;
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public Objective getObjective() {
        return objective;
    }

    public Team getScoreboardTeam(CDTeam team) {
        Preconditions.checkNotNull(team, "team");

        GameTeam team1 = teamMap.get(team);
        if (team1 == null) return null;

        return team1.getScoreboardTeam();
    }

    public List<Player> getPlayersOn(CDTeam team) {
        return Bukkit.getOnlinePlayers().stream().filter(p -> getPlayerTeam(p) == team).collect(Collectors.toList());
    }

    public CDTeam getPlayerTeam(Player player) {
        Preconditions.checkNotNull(player, "player");

        for (CDTeam team : CDTeam.values()) {
            Team team1 = getScoreboardTeam(team);
            Preconditions.checkState(team1 != null, "team must be valid");

            if (team1.hasEntry(player.getName())) return team;
        }

        return null;
    }

    public void putOnTeam(Player player, CDTeam team) {
        Preconditions.checkNotNull(player, "player");
        Preconditions.checkNotNull(team, "team");
        Preconditions.checkArgument(getPlayerTeam(player) == null, "player already has a team");

        teamMap.get(team).getScoreboardTeam().addEntry(player.getName());
    }

    public CDTeam assignTeam(Player player) {
        Preconditions.checkNotNull(player, "player");
        Preconditions.checkArgument(getPlayerTeam(player) == null, "player already has a team");

        Team savageTeam = teamMap.get(CDTeam.SAVAGE).getScoreboardTeam();
        Team protectorTeam = teamMap.get(CDTeam.PROTECTOR).getScoreboardTeam();

        CDTeam assigned;

        if (savageTeam.getEntries().size() > protectorTeam.getEntries().size()) {
            protectorTeam.addPlayer(player);
            assigned = CDTeam.PROTECTOR;
        } else if (savageTeam.getEntries().size() < protectorTeam.getEntries().size()) {
            savageTeam.addPlayer(player);
            assigned = CDTeam.SAVAGE;
        } else {
            assigned = CDTeam.values()[ControlPlugin.getPlugin().getRandom().nextInt(CDTeam.values().length)];
            teamMap.get(assigned).getScoreboardTeam().addPlayer(player);
        }

        return assigned;
    }

    public CheckpointState getCheckpointState(GameConfiguration.Checkpoint checkpoint) {
        Preconditions.checkNotNull(checkpoint, "checkpoint");
        Preconditions.checkArgument(checkpointMap.containsKey(checkpoint), "checkpoint not valid");

        return checkpointMap.get(checkpoint);
    }

    public GameRewardManager getRewardManager() {
        return rewardManager;
    }

    public static class GameTeam {
        private final Team scoreboardTeam;
        private final CDTeam enumTeam;

        public GameTeam(Team scoreboardTeam, CDTeam enumTeam) {
            this.scoreboardTeam = scoreboardTeam;
            this.enumTeam = enumTeam;
        }

        public Team getScoreboardTeam() {
            return scoreboardTeam;
        }

        public CDTeam getEnumTeam() {
            return enumTeam;
        }
    }

    public static class CheckpointState {
        private final GameConfiguration.Checkpoint checkpoint;
        private int health;
        private int lastAttack = -1;

        public CheckpointState(GameConfiguration.Checkpoint checkpoint) {
            this.checkpoint = checkpoint;
            this.health = checkpoint.getMaxHealth();
        }

        public GameConfiguration.Checkpoint getCheckpoint() {
            return checkpoint;
        }

        public int getHealth() {
            return health;
        }

        public void setHealth(int health) {
            this.health = health;
        }

        public boolean isDestroyed() {
            return health == 0;
        }

        public boolean isFullHealth() {
            return health == checkpoint.getMaxHealth();
        }

        public int getLastAttack() {
            return lastAttack;
        }

        public void setLastAttack(int lastAttack) {
            this.lastAttack = lastAttack;
        }
    }
}
