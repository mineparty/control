package io.mineparty.control.game.data;

import java.util.UUID;

import io.mineparty.commons.core.data.Statistic;


public class Statistics {
    
    public static final Statistic CD_GAME_WINS = Statistic.builder()
            .id(UUID.fromString("f2ef2d08-9b63-4d0f-9ef3-8cadfffa99a3"))
            .name("Games Won")
            .build();
    
    public static final Statistic CD_CHECKPOINT_CAPTURES = Statistic.builder()
            .id(UUID.fromString("a455220c-a0e4-4e47-b548-a92f025db2b5"))
            .name("Checkpoints Destroyed")
            .build();
    
    public static final Statistic CD_CHECKPOINT_CAPTURES_ALONE = Statistic.builder()
            .id(UUID.fromString("0ba667bb-c4de-40ea-a12e-603cfb5c4946"))
            .name("Checkpoints Destroyed Alone")
            .build();

    public static final Statistic CD_KILLS = Statistic.builder()
            .id(UUID.fromString("97679c19-8f79-40b0-83a4-457f8d9067ad"))
            .name("Kills")
            .build();

    public static final Statistic CD_DEATHS = Statistic.builder()
            .id(UUID.fromString("3e7b0a5b-99da-44f5-8f5a-e6baf042e242"))
            .name("Deaths")
            .build();

}
