package io.mineparty.control.game.data;

import io.mineparty.game.extras.rewards.GameRewardManager;

import java.util.Optional;

public class CDRewards {
    public static final GameRewardManager.Reward KILLS = GameRewardManager.Reward.builder()
            .coins(2)
            .name("kill")
            .achievement(Optional.empty())
            .statistic(Optional.of(Statistics.CD_KILLS))
            .build();

    public static final GameRewardManager.Reward DEATHS = GameRewardManager.Reward.builder()
            .name("death")
            .achievement(Optional.empty())
            .statistic(Optional.of(Statistics.CD_DEATHS))
            .build();

    public static final GameRewardManager.Reward CHECKPOINTS_CAPTURE = GameRewardManager.Reward.builder()
            .name("captured checkpoint")
            .coins(1)
            .achievement(Optional.empty())
            .statistic(Optional.of(Statistics.CD_CHECKPOINT_CAPTURES))
            .build();
}
