package io.mineparty.control.game.data;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.DyeColor;

public enum CDTeam {
    SAVAGE(ChatColor.RED, "Savages", "Capture the checkpoints and kill the Protectors!", Color.RED, DyeColor.RED),
    PROTECTOR(ChatColor.DARK_GREEN, "Protectors", "Defend the checkpoints from the Savages!", Color.GREEN, DyeColor.GREEN);

    private final ChatColor color;
    private final String name;
    private final String goal;
    private final Color fireworkColor;
    private final DyeColor dyeColor;

    CDTeam(ChatColor color, String name, String goal, Color fireworkColor, DyeColor dyeColor) {
        this.color = color;
        this.name = name;
        this.goal = goal;
        this.fireworkColor = fireworkColor;
        this.dyeColor = dyeColor;
    }

    public ChatColor getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public String getGoal() {
        return goal;
    }

    public Color getFireworkColor() {
        return fireworkColor;
    }

    @Override
    public String toString() {
        return color + name;
    }

    public DyeColor getDyeColor() {
        return dyeColor;
    }
}
