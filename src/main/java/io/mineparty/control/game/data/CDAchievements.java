package io.mineparty.control.game.data;

import java.util.UUID;

import io.mineparty.commons.core.data.Achievement;
import io.mineparty.commons.core.data.AchievementCategory;
import io.mineparty.control.game.achievement.*;

public class CDAchievements {
    public static final Achievement GOOD_MOURNING = Achievement.builder()
            .id(UUID.fromString("24b34cba-ac26-4e77-b44f-0a28ce7b0b79"))
            .name("Good Mourning")
            .category(AchievementCategory.CONTROL)
            .description("Win your first game of Control.")
            .build();

    public static final Achievement DISRUPTED = Achievement.builder()
            .id(UUID.fromString("bd801884-4dd9-49f3-a725-cac2f39f24af"))
            .name("The Peace is Disrupted")
            .category(AchievementCategory.CONTROL)
            .description("Help capture a checkpoint.")
            .build();

    public static final Achievement PACIFIST = Achievement.builder()
            .id(UUID.fromString("59e5a570-84bc-4464-9c59-e7f7384db6f2"))
            .name("The Pacifist")
            .category(AchievementCategory.CONTROL)
            .description("Win a game without killing anyone.")
            .build();

    public static final Achievement NO_HELP_REQUIRED = Achievement.builder()
            .id(UUID.fromString("5574efad-4da5-4f07-9c0f-a33d4f3c34b3"))
            .name("No Help Required")
            .category(AchievementCategory.CONTROL)
            .description("Successfully capture a checkpoint by yourself.")
            .build();

    public static final Achievement STRONG_ALONE = Achievement.builder()
            .id(UUID.fromString("41c4fcc2-634c-4341-bc88-74ae6836398b"))
            .name("Standing Strong Alone")
            .category(AchievementCategory.CONTROL)
            .description("Successfully capture 150 checkpoints by yourself.")
            .build();

    public static final Achievement CHECK_THAT_POINT = Achievement.builder()
            .id(UUID.fromString("7e5c88a6-79aa-4f11-a7c9-ca759228bf6c"))
            .name("Check that Point")
            .category(AchievementCategory.CONTROL)
            .description("Capture 250 checkpoints.")
            .build();

    public static final Achievement CHECKING_IN = Achievement.builder()
            .id(UUID.fromString("fd3c6713-9492-4251-867d-199c099ff8e9"))
            .name("Checking In")
            .category(AchievementCategory.CONTROL)
            .description("Capture 500 checkpoints.")
            .build();

    public static final Achievement CHECKPOINT_MASTER = Achievement.builder()
            .id(UUID.fromString("5983c440-72b7-4ea2-ba3a-be59f9d3bf9c"))
            .name("Checking for Victory")
            .category(AchievementCategory.CONTROL)
            .description("Capture 1,000 checkpoints.")
            .build();

    public static final Achievement CHECKMATE = Achievement.builder()
            .id(UUID.fromString("7685d010-c07b-4bc2-8f11-79dde49dc8e7"))
            .name("Checkmate")
            .category(AchievementCategory.CONTROL)
            .description("Capture 5,000 checkpoints.")
            .build();

    public static final CDAchievement WIN_FIRST_GAME = new WinGameAchievement(GOOD_MOURNING, 1,
            new AchievementReward[]{AchievementReward.builder().coins(25).build()}); //TODO better rewards
    
    public static final CDAchievement CAPTURE_FIRST_CHECKPOINT = new CaptureCheckpointAchievement(DISRUPTED, 1,
            new AchievementReward[]{AchievementReward.builder().coins(25).build()});

    public static final CDAchievement CAPTURE_CHECKPOINT_ALONE = new CaptureCheckpointAloneAchievement(NO_HELP_REQUIRED, 1,
            new AchievementReward[]{AchievementReward.builder().coins(30).build()});

    public static final CDAchievement CAPTURE_150_CHECKPOINTS_ALONE = new CaptureCheckpointAloneAchievement(STRONG_ALONE, 150,
            new AchievementReward[]{AchievementReward.builder().coins(2000).build()});

    public static final CDAchievement PACIFIST_GOAL = new PacifistAchievement(new AchievementReward[]{AchievementReward.builder().coins(500).build()});

    public static final CDAchievement CHECK_THAT_POINT_CHECK = new CaptureCheckpointAchievement(CHECK_THAT_POINT, 250,
            new AchievementReward[]{AchievementReward.builder().coins(250).build()});

    public static final CDAchievement CHECKING_IN_CHECKPOINT = new CaptureCheckpointAchievement(CHECKING_IN, 500,
            new AchievementReward[]{AchievementReward.builder().coins(500).build()});

    public static final CDAchievement CHECKPOINT_MASTER_CHECKPOINT = new CaptureCheckpointAchievement(CHECKPOINT_MASTER, 1000,
            new AchievementReward[]{AchievementReward.builder().coins(750).build()});

    public static final CDAchievement CHECKMATE_CHECKPOINT = new CaptureCheckpointAchievement(CHECKMATE, 5000,
            new AchievementReward[]{AchievementReward.builder().coins(1000).build()});
}
