package io.mineparty.control.game;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.game.data.CDTeam;
import io.mineparty.lib.ItemBuilder;
import io.mineparty.lib.gui.GlobalGUI;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class TeamGUI extends GlobalGUI {
    private static final CDTeam[] TEAMS = CDTeam.values();
    @Getter
    private final Multimap<CDTeam, UUID> members = HashMultimap.create();

    private int getMaxTeamLimit() {
        return Bukkit.getOnlinePlayers().size() / CDTeam.values().length;
    }

    private ItemBuilder getBuilder(CDTeam team) {
        ItemBuilder builder = new ItemBuilder(Material.STAINED_CLAY).setDyeColor(team.getDyeColor()).setRawTitle(team.toString() +
                ChatColor.GRAY + " - " + members.keys().count(team) + "/" + getMaxTeamLimit() + " players");

        for (UUID uuid : members.get(team)) {
            builder.addLore("- " + Bukkit.getPlayer(uuid).getName());
        }

        return builder;
    }

    public ItemStack getItem() {
        ItemStack stack = new ItemStack(Material.STAINED_CLAY, 1, (short) DyeColor.CYAN.getWoolData());
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "Select Team");
        meta.setLore(ImmutableList.of(ChatColor.WHITE + "Right-click to select a team."));
        stack.setItemMeta(meta);
        return stack;
    }

    public TeamGUI() {
        super(ControlPlugin.getPlugin(), 9, "Select Team");
        update();
    }

    public void update() {
        for (int i = 0; i < TEAMS.length; i++) {
            CDTeam team = TEAMS[i];
            setItem(i, getBuilder(team), p -> {
                        if (members.keys().count(team) >= getMaxTeamLimit()) {
                            Bukkit.getPlayer(p.getMinecraftUuid()).sendMessage(ChatColor.RED + "Too many people on this team!");
                            return;
                        }
                        members.put(team, p.getMinecraftUuid());
                        Bukkit.getPlayer(p.getMinecraftUuid()).sendMessage(ChatColor.GRAY + "Now on the " + team + ChatColor.GRAY + " team!");

                        update();
                    });
        }
    }
}
