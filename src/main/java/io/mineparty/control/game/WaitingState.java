package io.mineparty.control.game;

import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.control.ControlPlugin;
import io.mineparty.control.game.data.GameData;
import io.mineparty.control.game.kit.CDKits;
import io.mineparty.game.GameState;
import io.mineparty.game.MPGame;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class WaitingState extends io.mineparty.game.extras.states.WaitingState {
    @Getter
    private final TeamGUI teamGUI = new TeamGUI();

    @Override
    public void onStart(GameState previous) {
        ControlPlugin.getPlugin().setGameData(new GameData(MPGame.getPlugin().getSelectedMap(), MPGame.getPlugin().getGameWorld()));
        super.onStart(previous);
    }

    @Override
    public void onStop(GameState next) {
        super.onStop(next);
    }

    @Override
    protected void setupPlayer(Player player) {
        super.setupPlayer(player);
        player.getInventory().setItem(0, CDKits.getKitItem());
        player.getInventory().setItem(8, teamGUI.getItem());

        player.sendMessage(ChatColor.GREEN + "Welcome to the game! Please wait until the game starts.");
        player.sendMessage(ChatColor.GREEN + "While you wait, please select your kit by right-clicking the iron sword.");
    }

    @EventHandler
    public void onPlayerQuit_CWS(PlayerQuitEvent event) {
        teamGUI.getMembers().values().remove(event.getPlayer().getUniqueId());
        teamGUI.update();
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(true);

        if (event.getMaterial() == Material.IRON_SWORD) {
            ControlPlugin.getPlugin().getGameData().getKitManager().openMenu(MinePartyCommons.getPlugin()
                    .getPlayer(event.getPlayer()));
        } else if (event.getMaterial() == Material.STAINED_CLAY) {
            teamGUI.open(MinePartyCommons.getPlugin().getPlayer(event.getPlayer()));
        }
    }
}
