package io.mineparty.control.event;

import java.util.List;

import io.mineparty.control.configuration.GameConfiguration;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import io.mineparty.commons.core.MinePartyPlayer;

public class CheckpointCaptureEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    private final GameConfiguration.Checkpoint checkpoint;
    private final List<MinePartyPlayer> players;

    public CheckpointCaptureEvent(GameConfiguration.Checkpoint checkpoint, List<MinePartyPlayer> players) {
        this.checkpoint = checkpoint;
        this.players = players;
    }

    public CheckpointCaptureEvent(boolean isAsync, GameConfiguration.Checkpoint checkpoint, List<MinePartyPlayer> players) {
        super(isAsync);
        this.checkpoint = checkpoint;
        this.players = players;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }

    public GameConfiguration.Checkpoint getCheckpoint() {
        return checkpoint;
    }

    public List<MinePartyPlayer> getPlayers() {
        return players;
    }
}
