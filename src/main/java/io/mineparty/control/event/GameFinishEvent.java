package io.mineparty.control.event;

import java.util.List;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import io.mineparty.commons.core.MinePartyPlayer;

public class GameFinishEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final List<MinePartyPlayer> winners;

    public GameFinishEvent(List<MinePartyPlayer> winners) {
        this.winners = winners;
    }

    public GameFinishEvent(boolean isAsync, List<MinePartyPlayer> winners) {
        super(isAsync);
        this.winners = winners;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }

    public List<MinePartyPlayer> getWinners() {
        return winners;
    }

}
