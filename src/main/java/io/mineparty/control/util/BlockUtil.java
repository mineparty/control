package io.mineparty.control.util;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;
import java.util.List;

public class BlockUtil {
    private BlockUtil() {

    }

    public static List<Block> searchUpwards(Block base, Material material) {
        List<Block> blocks = new ArrayList<>();

        while (base.getType() == material) {
            blocks.add(base);
            base = base.getRelative(BlockFace.UP);
        }

        return blocks;
    }
}
